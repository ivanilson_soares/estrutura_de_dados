#include <stdio.h>
#include "pilha.h"

int main(){
  int opc, iguais = 1;
	char valor;
  Pilha *p;
  do{
	printf("Digite a opcao desejada: \n 1- Criar Pilha \n 2- Inserir \n 3 - inverter \n 4 - Sair\n");
    	scanf("%d", &opc);
	
	switch(opc){
	    case 1:
             p = criar();
 		    printf("Pilha criada com sucesso\n");
 	    break;
	    case 2:
 	        printf("Digite o valor a ser inserido na Pilha\n");
 		    scanf("%s", &valor);
            inserir(p, (int)valor);
	    break;
        case 3:
 	        inverter(p);
        
	    break;
        default: printf("Opcap Invalida");
	}
  }while(opc != 4);
  return(0);
}