//#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"
#define N 20

struct pilha{
   int v[N];
   int topo;
};
   Pilha *aux;
Pilha* criar(){
   Pilha *p = (Pilha*) malloc(sizeof(Pilha));
   p->topo = -1;
    return(p);
}

int cheia(Pilha *p){
   if(p->topo == N-1) return 1;
   else return(0);
}

void inserir(Pilha *p, int valor){
   if(p->topo == N-1) printf("A pilha esta cheia");
   else{
      p->topo = p->topo+1;
      p->v[p->topo] = valor;
   }
}

int vazia(Pilha *p){
  if(p->topo ==-1) return 1;
  else return 0;
}

int remover(Pilha *p){
   int valor;
   if(vazia(p)){ printf("A pilha está vazia, nao é possivel fazer remocao");
    exit(1);
   }
   valor = p-> v[p->topo];
   p->topo = p->topo-1;
   return(valor); 
}

void imprimir(Pilha *p){

    printf("\nO elemento  do topo da pilha e  %d",p->v[p->topo]);     

}

void inverter(Pilha *p){
   aux = criar();
      int iguais = 1;
   while(!vazia(p)){
      inserir(aux, remover(p));
   }
   while(iguais && !vazia(p) && !vazia(aux)){
      if(remover(p) != remover(aux)) iguais = 0;
   }   
   if(vazia(p) && vazia(aux)) printf("\nE um polidromo %d\n", iguais);
   else printf("\nNao e polidromo %d \n",iguais);
} 

void liberar(Pilha *p){
  free(p);
}
